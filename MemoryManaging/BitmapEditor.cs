﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace MemoryManaging
{

    public interface IBitmapEditor : IDisposable
    {
        void SetPixel(int x, int y, byte R, byte G, byte B);
        Color GetPixel(int x, int y);
    }

    public class BitmapEditor : IBitmapEditor
    {
        private Bitmap bitmap;
        private static byte RGB_COUNT = 3;
        private static byte R_OFFSET = 2;
        private static byte G_OFFSET = 1;
        private static byte B_OFFSET = 0;

        public BitmapEditor(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }


        public Color GetPixel(int x, int y)
        {
            Color color;

            Rectangle rectangle = new Rectangle(x, y, 1, 1);
            var lockedBits = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
            var pointerToFirstPixel = lockedBits.Scan0;
            var position = getPositionByXY(x, y);
            var bgrValues = new byte[RGB_COUNT];
            Marshal.Copy(pointerToFirstPixel, bgrValues, 0, RGB_COUNT);
            color = Color.FromArgb(
                bgrValues[position + R_OFFSET],
                bgrValues[position + G_OFFSET],
                bgrValues[position + B_OFFSET]);
            bitmap.UnlockBits(lockedBits);
            return color;
        }

        public void SetPixel(int x, int y, byte R, byte G, byte B)
        {
            var rectangle = new Rectangle(x, y, 1, 1);
            var lockedBits = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
            var pointerToFirstPixel = lockedBits.Scan0;
            var bgrValues = new byte[RGB_COUNT];
            Marshal.Copy(pointerToFirstPixel, bgrValues, 0, RGB_COUNT);
            var position = getPositionByXY(x, y);
            bgrValues[position + R_OFFSET] = R;
            bgrValues[position + G_OFFSET] = G;
            bgrValues[position + B_OFFSET] = B;
            Marshal.Copy(bgrValues, 0, pointerToFirstPixel, RGB_COUNT);
            bitmap.UnlockBits(lockedBits);
        }

        private int getPositionByXY(int x, int y)
        {
            return x * Math.Abs(bitmap.Width) + y;
        }

        public void Dispose()
        {
            bitmap = null;
        }
    }
}
