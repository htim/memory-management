﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MemoryManaging
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer = new Timer();
            using (timer.Start())
            {
                var req = WebRequest.Create("https://google.com");
                var res = req.GetResponse();
            }
            Console.WriteLine(timer.EllapsedMilliseconds);
            using (timer.Continue())
            {
                var req = WebRequest.Create("https://ya.ru");
                var res = req.GetResponse();
            }
            Console.WriteLine(timer.EllapsedMilliseconds);

            var bitmap = (Bitmap)Bitmap.FromFile("pic.jpg");

            using (var bitmapEditor = new BitmapEditor(bitmap))
            {
                var colorBefore = bitmapEditor.GetPixel(0, 0);
                bitmapEditor.SetPixel(0, 0, 0, 0, 0);
                var colorAfter = bitmapEditor.GetPixel(0, 0);
            }
        }
    }
}
