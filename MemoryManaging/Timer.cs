﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryManaging
{
    public interface ITimer
    {

    }

    public class Timer
    {
        private DisposableTimer disposableTimer;

        public Timer()
        {
            disposableTimer = new DisposableTimer();
        }

        public DisposableTimer Start()
        {
            disposableTimer.Start();
            return disposableTimer;
        }

        public DisposableTimer Continue()
        {
            disposableTimer.Continue();
            return disposableTimer;
        }

        public long EllapsedMilliseconds
        {
            get { return disposableTimer.EllapsedMilliseconds; }
        }

    }

    public class DisposableTimer : IDisposable
    {

        private Stopwatch stopwatch;

        public DisposableTimer()
        {
            stopwatch = new Stopwatch();
        }

        public void Start()
        {
            stopwatch.Start();
        }

        public void Continue()
        {
            stopwatch.Start();
        }

        public long EllapsedMilliseconds
        {
            get { return stopwatch.ElapsedMilliseconds; }
        }

        public void Dispose()
        {
            stopwatch.Stop();
        }
    }




}
